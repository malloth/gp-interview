package uk.co.grandparade.interview.extension

import android.view.LayoutInflater
import android.view.ViewGroup

internal fun ViewGroup.inflate(resId: Int) = LayoutInflater.from(context).inflate(resId, this, false)