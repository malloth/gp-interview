package uk.co.grandparade.interview.activity

import android.content.Intent
import android.os.Bundle
import com.microsoft.graph.concurrency.ICallback
import com.microsoft.graph.core.ClientException
import uk.co.grandparade.interview.R
import uk.co.grandparade.interview.app.AppActivity
import kotlinx.android.synthetic.main.activity_login.button_login as loginButton
import kotlinx.android.synthetic.main.activity_login.view_switcher as viewSwitcher

class LoginActivity : AppActivity() {

    companion object {

        private const val CHILD_BUTTON = 0
        private const val CHILD_PROGRESS = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener { login() }
    }

    private fun login() {
        showProgress()
        authenticator.login(this, object : ICallback<Void> {
            override fun success(result: Void?) {
                showLoginButton()
                startUserActivity()
            }

            override fun failure(ex: ClientException) {
                ex.printStackTrace()
                showLoginButton()
            }
        })
    }

    private fun showProgress() {
        viewSwitcher.displayedChild = CHILD_PROGRESS
    }

    private fun showLoginButton() {
        viewSwitcher.displayedChild = CHILD_BUTTON
    }

    private fun startUserActivity() {
        startActivity(Intent(this, UserActivity::class.java))
    }
}