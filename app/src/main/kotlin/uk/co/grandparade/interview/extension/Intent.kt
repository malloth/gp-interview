package uk.co.grandparade.interview.extension

import android.content.Intent
import android.os.Parcelable

internal inline fun <reified T : Parcelable> Intent.getArrayExtra(key: String): Array<T>? {
    val parcelables = getParcelableArrayExtra(key) ?: return null
    return Array(parcelables.size, { parcelables[it] as T })
}

internal inline fun <reified T : Parcelable> Intent.getArrayExtra(key: String, default: Array<T>): Array<T> {
    val parcelables = getParcelableArrayExtra(key) ?: return default
    return Array(parcelables.size, { parcelables[it] as T })
}