package uk.co.grandparade.interview.extension

import com.microsoft.graph.extensions.DriveItem

internal fun DriveItem.isWorksheet() = name.endsWith(".xlsx", true) ||
        name.endsWith(".xls", true)