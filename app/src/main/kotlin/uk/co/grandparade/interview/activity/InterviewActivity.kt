package uk.co.grandparade.interview.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.google.gson.JsonArray
import com.microsoft.graph.concurrency.ICallback
import com.microsoft.graph.core.ClientException
import com.microsoft.graph.extensions.IWorkbookWorksheetCollectionPage
import com.microsoft.graph.extensions.WorkbookRange
import kotlinx.android.synthetic.main.activity_interview.*
import uk.co.grandparade.interview.BuildConfig
import uk.co.grandparade.interview.R
import uk.co.grandparade.interview.app.AppActivity
import uk.co.grandparade.interview.extension.getArray
import uk.co.grandparade.interview.model.Challenge
import uk.co.grandparade.interview.model.Challenge.Results.RESULT_ALMOST
import uk.co.grandparade.interview.model.Challenge.Results.RESULT_BAD
import uk.co.grandparade.interview.model.Challenge.Results.RESULT_GOOD
import uk.co.grandparade.interview.model.Challenge.Results.RESULT_NONE
import kotlinx.android.synthetic.main.activity_interview.button_almost as almostButton
import kotlinx.android.synthetic.main.activity_interview.button_bad as badButton
import kotlinx.android.synthetic.main.activity_interview.button_good as goodButton
import kotlinx.android.synthetic.main.activity_interview.button_next as nextButton
import kotlinx.android.synthetic.main.activity_interview.questions_left as questionsLeft

class InterviewActivity : AppActivity() {

    companion object {

        const val SAVED_CHALLENGES = "CHALLENGES"
        const val SAVED_DONE = "DONE"
        const val SAVED_CURRENT = "CURRENT"

        const val EXTRA_NAME = "NAME"
        const val EXTRA_SURNAME = "SURNAME"
        const val EXTRA_PROFILE_EMAIL = "PROFILE_EMAIL"
        const val EXTRA_EMAIL = "EMAIL"
        const val EXTRA_WORKSHEET = "WORKSHEET"
        const val EXTRA_CATEGORY = "CATEGORY"
        const val EXTRA_COUNT = "COUNT"

        const val CATEGORY_ANDROID = "android"
        //const val CATEGORY_IOS = "ios"

        const val DEFAULT_CHALLENGE_COUNT = 30
    }

    private var challenges: Array<Challenge>? = null
    private var done: Int = 0
    private var current: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_interview)

        savedInstanceState?.let {
            challenges = it.getArray(SAVED_CHALLENGES)
            current = it.getInt(SAVED_CURRENT, -1)
            done = it.getInt(SAVED_DONE, 0)

            challenges?.let {
                showChallenge(it[current])
            }
        } ?: with(intent) {
            val worksheet = getStringExtra(EXTRA_WORKSHEET)
            val count = getIntExtra(EXTRA_COUNT, DEFAULT_CHALLENGE_COUNT)

            loadChallenges(worksheet, count)
        }
        goodButton.setOnClickListener(::handleButtonClick)
        almostButton.setOnClickListener(::handleButtonClick)
        badButton.setOnClickListener(::handleButtonClick)
        nextButton.setOnClickListener(::handleButtonClick)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.let {
            it.putParcelableArray(SAVED_CHALLENGES, challenges)
            it.putInt(SAVED_CURRENT, current)
            it.putInt(SAVED_DONE, done)
        }
    }

    private fun handleButtonClick(view: View) {
        when (view.id) {
            R.id.button_good,
            R.id.button_almost,
            R.id.button_bad -> {
                val mark = getMarkFromButton(view.id)
                markChallenge(mark)

                if (!showNextChallenge()) {
                    finish()
                    startResultActivity()
                }
            }
            R.id.button_next -> showNextChallenge()
        }
    }

    private fun loadChallenges(worksheet: String, questionCount: Int) {
        client.me.drive.root
                .getItemWithPath(worksheet)
                .workbook.worksheets
                .buildRequest()
                .get(object : ICallback<IWorkbookWorksheetCollectionPage> {

                    override fun success(result: IWorkbookWorksheetCollectionPage) {
                        if (result.currentPage.isEmpty()) {
                            finish()
                            return
                        }

                        val worksheetId = result.currentPage.first().id

                        client.me.drive.root
                                .getItemWithPath(worksheet)
                                .workbook
                                .getWorksheets(worksheetId)
                                .usedRange
                                .buildRequest()
                                .get(object : ICallback<WorkbookRange> {

                                    override fun success(result: WorkbookRange) {
                                        challenges = result.values.asJsonArray
                                                .map { it.asJsonArray }
                                                .mapNotNull { it.asChallenge }
                                                .filter { it.question.isNotEmpty() }
                                                .drop(1)
                                                .shuffled()
                                                .take(questionCount)
                                                .toTypedArray()

                                        challenges?.let {
                                            current = 0
                                            showChallenge(it[current])

                                            if (BuildConfig.DEBUG) {
                                                it.forEach { println(it) }
                                            }
                                        }
                                    }

                                    override fun failure(ex: ClientException) {
                                        ex.printStackTrace()
                                    }
                                })
                    }

                    override fun failure(ex: ClientException) {
                        ex.printStackTrace()
                    }
                })
    }

    private fun getMarkFromButton(id: Int) = when (id) {
        R.id.button_good -> RESULT_GOOD
        R.id.button_almost -> RESULT_ALMOST
        R.id.button_bad -> RESULT_BAD
        else -> throw AssertionError("Unknown button id: $id")
    }

    private fun markChallenge(mark: Float) = challenges?.let {
        it[current].result = mark
        done++
    }

    private fun showNextChallenge() = challenges?.let {
        val oldCurrent = current

        current = it.drop(current + 1)
                .indexOfFirst { it.result == RESULT_NONE }

        if (current == -1) {
            current = it.indexOfFirst { it.result == RESULT_NONE }
        } else if (oldCurrent > -1) {
            current += oldCurrent + 1
        }

        if (current != -1) {
            showChallenge(it[current])
            true
        } else {
            false
        }
    } ?: false

    private fun showChallenge(challenge: Challenge) {
        progress.visibility = GONE
        question_panel.visibility = VISIBLE
        button_panel.visibility = VISIBLE

        category.text = challenge.category
        difficulty.text = challenge.difficulty
        questionText.setText(challenge.question)
        questionsLeft.text = getString(R.string.label_challenges_done, done, challenges?.size ?: 0)
    }

    private fun startResultActivity() {
        val oldIntent = intent
        val intent = Intent(this, ResultActivity::class.java)

        with(intent) {
            putExtra(ResultActivity.EXTRA_NAME, oldIntent.getStringExtra(EXTRA_NAME))
            putExtra(ResultActivity.EXTRA_SURNAME, oldIntent.getStringExtra(EXTRA_SURNAME))
            putExtra(ResultActivity.EXTRA_PROFILE_EMAIL, oldIntent.getStringExtra(EXTRA_PROFILE_EMAIL))
            putExtra(ResultActivity.EXTRA_EMAIL, oldIntent.getStringExtra(EXTRA_EMAIL))
            putExtra(ResultActivity.EXTRA_CHALLENGES, challenges)
        }
        startActivity(intent)
    }

    private val JsonArray.asChallenge: Challenge?
        get() {
            val difficulty = get(0).asString ?: return null
            val question = if (size() > 1) get(1).asString else ""
            val answer = if (size() > 2) get(2).asString else null
            val category = if (size() > 3) get(3).asString else null

            return Challenge(question, answer, category, difficulty)
        }
}