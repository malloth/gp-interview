package uk.co.grandparade.interview.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.view.inputmethod.EditorInfo.IME_NULL
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import com.microsoft.graph.concurrency.ICallback
import com.microsoft.graph.core.ClientException
import com.microsoft.graph.extensions.IDriveItemCollectionPage
import com.microsoft.graph.extensions.User
import kotlinx.android.synthetic.main.activity_user.*
import uk.co.grandparade.interview.BuildConfig
import uk.co.grandparade.interview.R
import uk.co.grandparade.interview.app.AppActivity
import uk.co.grandparade.interview.extension.inflate
import uk.co.grandparade.interview.extension.isValidEmail
import uk.co.grandparade.interview.extension.isWorksheet
import uk.co.grandparade.interview.extension.setOnAfterTextChangedListener
import kotlinx.android.synthetic.main.activity_user.button_start as startButton
import kotlinx.android.synthetic.main.activity_user.email_layout as emailLayout
import kotlinx.android.synthetic.main.activity_user.interviewer_email as profileEmail
import kotlinx.android.synthetic.main.activity_user.interviewer_name as profileName
import kotlinx.android.synthetic.main.activity_user.interviewer_photo as profilePhoto
import kotlinx.android.synthetic.main.activity_user.name_layout as nameLayout
import kotlinx.android.synthetic.main.activity_user.surname_layout as surnameLayout
import kotlinx.android.synthetic.main.activity_user.worksheet_hint as worksheetSelectorHint
import kotlinx.android.synthetic.main.activity_user.worksheet_selector as worksheetSelector
import kotlinx.android.synthetic.main.activity_user.worksheet_selector_label as worksheetSelectorLabel

class UserActivity : AppActivity() {

    companion object {

        const val SAVED_WORKSHEETS = "WORKSHEETS"
    }

    private class WorksheetAdapter(private var worksheets: Array<String> = emptyArray()) : BaseAdapter() {

        override fun getCount() = worksheets.size

        override fun getItem(position: Int) = worksheets[position]

        override fun getItemId(position: Int) = worksheets[position].hashCode().toLong()

        override fun getView(position: Int,
                             convertView: View?,
                             parent: ViewGroup) =
                getView(position, convertView, parent, android.R.layout.simple_spinner_item)

        override fun getDropDownView(position: Int,
                                     convertView: View?,
                                     parent: ViewGroup) =
                getView(position, convertView, parent, android.R.layout.simple_spinner_dropdown_item)

        fun updateWorksheets(worksheets: Array<String>) {
            this.worksheets = worksheets
            notifyDataSetChanged()
        }

        private fun getView(position: Int,
                            convertView: View?,
                            parent: ViewGroup,
                            layoutResId: Int): View {
            val view = convertView ?: parent.inflate(layoutResId)

            if (view is TextView) {
                view.text = worksheets[position]
            }
            return view
        }
    }

    private var worksheets: Array<String>? = null
    private val worksheetAdapter = WorksheetAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_user)

        name.setOnAfterTextChangedListener { validateFormData(name) }
        surname.setOnAfterTextChangedListener { validateFormData(surname) }
        email.setOnAfterTextChangedListener { validateFormData(email) }
        email.setOnEditorActionListener { _, id, _ -> performEditorAction(id) }

        with(worksheetSelector) {
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(adapterView: AdapterView<*>,
                                            view: View,
                                            position: Int,
                                            id: Long) {
                    validateFormData(null)
                }

                override fun onNothingSelected(adapterView: AdapterView<*>) {}
            }
            onFocusChangeListener = View.OnFocusChangeListener { _, isFocused ->
                with(worksheetSelectorLabel) {
                    text = if (isFocused) hint else null
                }
            }
            adapter = worksheetAdapter
            isEnabled = false
        }

        startButton.setOnClickListener {
            if (it.isEnabled) {
                startInterviewActivity()
                clearFormData()
            }
        }

        savedInstanceState?.let {
            worksheets = it.getStringArray(SAVED_WORKSHEETS)
        } ?: {
            loadProfileData()

            if (BuildConfig.DEBUG) {
                name.setText(R.string.debug_name)
                surname.setText(R.string.debug_surname)
                email.setText(R.string.debug_email)
                validateFormData(null)
            }
        }.invoke()

        worksheets?.let {
            worksheetSelectorHint.visibility = GONE
            worksheetSelector.isEnabled = !it.isEmpty()
            worksheetAdapter.updateWorksheets(it)
        } ?: loadWorksheets()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putStringArray(SAVED_WORKSHEETS, worksheets)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        validateFormData(null)
    }

    override fun onBackPressed() {
        logout()
    }

    private fun loadProfileData() {
        client.me.buildRequest().get(object : ICallback<User> {
            override fun success(user: User) {
                profileName.text = user.displayName ?: ""
                profileEmail.text = user.userPrincipalName
            }

            override fun failure(ex: ClientException) {
                ex.printStackTrace()
            }
        })
        /*profilePhoto.viewTreeObserver.addOnGlobalLayoutListener( object: ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                profilePhoto.viewTreeObserver.removeOnGlobalLayoutListener(this)

                val photoId = "${profilePhoto.width}x${profilePhoto.height}"

                client.me.getPhotos(photoId).content.buildRequest().get(object : ICallback<InputStream> {

                    override fun success(inputStream: InputStream) {
                        inputStream.use {
                            BitmapFactory.decodeStream(it)?.let {
                                profilePhoto.setImageBitmap(it)
                            }
                        }
                    }

                    override fun failure(ex: ClientException) {
                        //ex.printStackTrace()
                    }
                })
            }
        })*/
    }

    private fun loadWorksheets() {
        client.me.drive.root.children.buildRequest().get(object : ICallback<IDriveItemCollectionPage> {

            override fun success(result: IDriveItemCollectionPage) {
                result.currentPage.filter { it.isWorksheet() }
                        .map { it.name }
                        .toTypedArray()
                        .let {
                            worksheets = it
                            worksheetSelectorHint.visibility = GONE
                            worksheetSelector.isEnabled = !it.isEmpty()
                            worksheetAdapter.updateWorksheets(it)
                        }
            }

            override fun failure(ex: ClientException) {
                ex.printStackTrace()
            }
        })
    }

    private fun validateFormData(fromView: View?) {
        val nameError = validateName()
        val surnameError = validateSurname()
        val emailError = validateEmail()
        val hasWorksheets = validateWorksheets()

        startButton.isEnabled = nameError == null &&
                surnameError == null &&
                emailError == null &&
                hasWorksheets

        when (fromView) {
            name -> {
                nameLayout.error = nameError
                nameLayout.isErrorEnabled = nameError != null
            }
            surname -> {
                surnameLayout.error = surnameError
                surnameLayout.isErrorEnabled = surnameError != null
            }
            email -> {
                emailLayout.error = emailError
                emailLayout.isErrorEnabled = emailError != null
            }
        }
    }

    private fun validateName() = with(name.text) {
        when {
            isNullOrEmpty() -> getString(R.string.error_empty_name)
            else -> null
        }
    }

    private fun validateSurname() = with(surname.text) {
        when {
            isNullOrEmpty() -> getString(R.string.error_empty_surname)
            else -> null
        }
    }

    private fun validateEmail() = with(email.text) {
        when {
            isNullOrEmpty() -> null
            isValidEmail() -> getString(R.string.error_invalid_email_format)
            else -> null
        }
    }

    private fun validateWorksheets() = worksheetSelector.selectedItem != null

    private fun performEditorAction(actionId: Int) = when (actionId) {
        IME_ACTION_DONE, IME_NULL -> {
            if (startButton.isEnabled) {
                startInterviewActivity()
                clearFormData()
            }
            true
        }
        else -> false
    }

    private fun startInterviewActivity() {
        val intent = Intent(this, InterviewActivity::class.java)

        with(intent) {
            putExtra(InterviewActivity.EXTRA_NAME, name.text.toString())
            putExtra(InterviewActivity.EXTRA_SURNAME, surname.text.toString())
            putExtra(InterviewActivity.EXTRA_PROFILE_EMAIL, profileEmail.text.toString())
            putExtra(InterviewActivity.EXTRA_EMAIL, email.text.toString())
            putExtra(InterviewActivity.EXTRA_WORKSHEET, worksheetSelector.selectedItem as String)
            putExtra(InterviewActivity.EXTRA_CATEGORY, InterviewActivity.CATEGORY_ANDROID)
            putExtra(InterviewActivity.EXTRA_COUNT, InterviewActivity.DEFAULT_CHALLENGE_COUNT)
        }
        startActivity(intent)
    }

    private fun clearFormData() {
        name.text = null
        name.requestFocus()
        nameLayout.isErrorEnabled = false

        surname.text = null
        surnameLayout.isErrorEnabled = false

        email.text = null
        emailLayout.isErrorEnabled = false
    }

    private fun logout() {
        authenticator.logout(object : ICallback<Void> {
            override fun success(result: Void?) {
                finish()
            }

            override fun failure(ex: ClientException) {
                ex.printStackTrace()
            }
        })
    }
}