package uk.co.grandparade.interview.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class Challenge(val question: String,
                     val answer: String? = null,
                     val category: String? = null,
                     val difficulty: String? = null,
                     var result: Float = RESULT_NONE) : Parcelable {

    companion object Results {

        const val RESULT_GOOD = 1F
        const val RESULT_ALMOST = .5F
        const val RESULT_BAD = 0F
        const val RESULT_NONE = -1F
    }
}