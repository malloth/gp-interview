package uk.co.grandparade.interview.extension

import android.os.Bundle
import android.os.Parcelable

internal inline fun <reified T : Parcelable> Bundle.getArray(key: String): Array<T>? {
    val parcelables = getParcelableArray(key) ?: return null
    return Array(parcelables.size, { parcelables[it] as T })
}

internal inline fun <reified T : Parcelable> Bundle.getArray(key: String, default: Array<T>): Array<T> {
    val parcelables = getParcelableArray(key) ?: return default
    return Array(parcelables.size, { parcelables[it] as T })
}