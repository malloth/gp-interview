package uk.co.grandparade.interview.app

import android.support.v7.app.AppCompatActivity
import com.microsoft.graph.core.DefaultClientConfig.createWithAuthenticationProvider
import com.microsoft.graph.extensions.GraphServiceClient
import com.microsoft.graph.extensions.IGraphServiceClient

abstract class AppActivity : AppCompatActivity() {

    protected val authenticator: AppAuthenticator
        get() = (application as AppApplication).authenticator

    protected val client: IGraphServiceClient by lazy {
        GraphServiceClient.Builder()
                .fromConfig(createWithAuthenticationProvider(authenticator))
                .buildClient()
    }
}