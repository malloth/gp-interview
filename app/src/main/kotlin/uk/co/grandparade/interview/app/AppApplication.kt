package uk.co.grandparade.interview.app

import android.app.Application

class AppApplication : Application() {

    val authenticator by lazy { AppAuthenticator(this) }
}