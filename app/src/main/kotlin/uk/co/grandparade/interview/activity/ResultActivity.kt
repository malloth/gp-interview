package uk.co.grandparade.interview.activity

import android.os.Bundle
import com.microsoft.graph.concurrency.ICallback
import com.microsoft.graph.core.ClientException
import com.microsoft.graph.extensions.*
import uk.co.grandparade.interview.BuildConfig
import uk.co.grandparade.interview.R
import uk.co.grandparade.interview.app.AppActivity
import uk.co.grandparade.interview.extension.getArrayExtra
import uk.co.grandparade.interview.model.Challenge
import kotlinx.android.synthetic.main.activity_result.button_done as doneButton
import kotlinx.android.synthetic.main.activity_result.name_surname as nameSurname
import kotlinx.android.synthetic.main.activity_result.result_image as resultImage
import kotlinx.android.synthetic.main.activity_result.total_score as totalScore
import kotlinx.android.synthetic.main.activity_result.view_switcher as viewSwitcher

class ResultActivity : AppActivity() {

    companion object {

        const val EXTRA_NAME = "NAME"
        const val EXTRA_SURNAME = "SURNAME"
        const val EXTRA_PROFILE_EMAIL = "PROFILE_EMAIL"
        const val EXTRA_EMAIL = "EMAIL"
        const val EXTRA_CHALLENGES = "CHALLENGES"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_result)

        doneButton.setOnClickListener { finish() }

        lateinit var name: String
        lateinit var surname: String
        lateinit var profileEmail: String
        lateinit var email: String
        lateinit var challenges: Array<Challenge>

        with(intent) {
            name = getStringExtra(EXTRA_NAME)
            surname = getStringExtra(EXTRA_SURNAME)
            profileEmail = getStringExtra(EXTRA_PROFILE_EMAIL)
            email = getStringExtra(EXTRA_EMAIL)
            challenges = getArrayExtra(EXTRA_CHALLENGES, emptyArray())
        }

        val fullName = "$name $surname"
        val score: Float = challenges.fold(0F, { total, next -> total + next.result })
        val maxScore = challenges.size * 1F

        resultImage.setImageResource(getResultImage(score, maxScore))
        nameSurname.text = fullName
        totalScore.text = getString(R.string.label_user_score, score, maxScore)

        val body = composeEmail(fullName, score, maxScore, challenges)

        sendEmailWithResults(profileEmail, email, getString(R.string.email_subject), body)
    }

    private fun getResultImage(score: Float, maxScore: Float) = when (score) {
        in maxScore * 0.8F..maxScore -> R.drawable.ic_thumb_up
        in maxScore * 0.5F..maxScore * 0.8F -> R.drawable.ic_thumbs_up_down
        else -> R.drawable.ic_thumb_down
    }

    private fun composeEmail(name: String,
                             score: Float,
                             maxScore: Float,
                             challenges: Array<Challenge>): String {
        val email = StringBuilder()

        email.append("<html>")
                .append("<body>")
                .appendResource(R.string.email_body1, name)
                .append("<br/><br/>")
                .appendResource(R.string.email_body2, "<b>$score</b>", "<b>$maxScore</b>")
                .append("<br/><br/>")
                .appendResource(R.string.email_body3)
                .append("<br/><br/>")
                .apply {
                    challenges.forEach {
                        append("<i>")
                        append(it.question)
                        append("</i>")
                        append(" - ")
                        append("<b>")
                        appendResource(R.string.label_user_score, it.result, 1F)
                        append("</b>")
                        append("<br/><br/>")
                    }
                }
                .appendResource(R.string.email_body4)
                .append("<br/><br/>")
                .append("----------")
                .append("<br/>")
                .appendResource(R.string.email_body5)
                .append("<br/>")
                .appendResource(R.string.email_body6)
                .append("</body>")
                .append("</html>")

        if (BuildConfig.DEBUG) {
            print(email)
        }
        return email.toString()
    }

    private fun sendEmailWithResults(from: String,
                                     to: String,
                                     subject: String,
                                     body: String) {
        val message = Message()
        message.toRecipients = if (to.isEmpty()) listOf(from.asRecipient) else listOf(from.asRecipient, to.asRecipient)
        message.subject = subject
        message.body = body.asHtmlBody

        client.me.getSendMail(message, true)
                .buildRequest()
                .post(object : ICallback<Void> {
                    override fun success(result: Void?) {
                        viewSwitcher.showNext()
                    }

                    override fun failure(ex: ClientException) {
                        ex.printStackTrace()
                        viewSwitcher.showNext()
                    }
                })
    }

    private val String.asRecipient: Recipient
        get() {
            val emailAddress = EmailAddress()
            emailAddress.address = this

            val recipient = Recipient()
            recipient.emailAddress = emailAddress

            return recipient
        }

    private val String.asHtmlBody: ItemBody
        get() {
            val itemBody = ItemBody()
            itemBody.content = this
            itemBody.contentType = BodyType.html

            return itemBody
        }

    private fun StringBuilder.appendResource(resId: Int, vararg args: Any) = append(getString(resId, *args))
}