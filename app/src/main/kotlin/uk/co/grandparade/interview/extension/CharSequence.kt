package uk.co.grandparade.interview.extension

import android.util.Patterns

internal fun CharSequence.isValidEmail() = !Patterns.EMAIL_ADDRESS.matcher(this).matches()