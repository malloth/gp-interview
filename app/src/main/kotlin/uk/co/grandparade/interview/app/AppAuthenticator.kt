package uk.co.grandparade.interview.app

import android.app.Application
import com.microsoft.graph.authentication.MSAAuthAndroidAdapter

class AppAuthenticator(application: Application) : MSAAuthAndroidAdapter(application) {

    override fun getScopes() = arrayOf("https://graph.microsoft.com/User.Read",
                                       "https://graph.microsoft.com/Mail.Send",
                                       "https://graph.microsoft.com/Files.Read")

    override fun getClientId() = "5e8715c9-faf5-4843-8ebe-743d3adc5c32"

    //override fun getClientId() = "6c02ce7b-1cf9-4d30-a7a0-e50c45f8af25"
}